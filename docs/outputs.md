#Výstup:
- Dvě celá čísla oddělená mezerou, která určují indexy souřadnic v hracím poli (2D pole po řádcích)
- V poli je stejný počet 'o' a 'x' (12), na tahu je 'o'
- Hráč 'o' hru vyhrává, když hraje na první řádek do třetího sloupce, tj. index v poli '0 2'

##Příklad vstupu 1: 
o . o x . o o  
. o . x x . .  
x . o o x . .  
o o . o x . x  
. . x . . x x  
> Na tahu je hráč o, vítězná řada je v diagonále  
> Výstup: 4 4  

##Příklad vstupu 2: 
x . . o o  
. . . . .  
o . x o .  
o . x . .  
o x x x o  
. o x . .  
> Na tahu je hráč x, doplnit může řadu ve třetím sloupci  
> Výstup: 1 2   

##Příklad vstupu 3: 
x x . . . .  
x o . o . .  
x o o x o .  
. o o . o x  
o . x x o .  
. x . . x .  
> Výstup: 0 4   
 
##Příklad vstupu 4: 
. o o . . o . .  
x o . x x . . .  
. . o x . . . .  
o . . . x . . o  
x o x o . o . x  
. x x o . o o .  
. . x o . o x .  
. x x . x . . o  
> Výstup 3 2   


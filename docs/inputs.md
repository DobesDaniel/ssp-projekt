#Vstup:
- přes příkazovou řádku bude programu předána cesta k souboru s hracím polem
- Soubor obsahuje znaky '.', 'x' a 'o' které značí prázdné pole, křížek (malé písmeno x) a kolečko (malé písmeno o).
- Znaky na řádce jsou oddělené mezerou, jiné znaky se v souborech nevyskytují. Pole ale nemusí nutně být čtvercové.
- Hráč s 'o' vždy začíná, tedy pro určení hráče na tahu platí, pokud je v poli stejný počet 'o' a 'x', pak je na tahu hráč 'o', pokud je počtů 'x' o jednu méně, než počtů 'o', pak hraje hráč 'x'

##Příklad vstupního souboru:
o . . o o o o  
x o . x x . x  
x . o . . x o  
o x o x o o x  
x . . x x . .  


# SSP-projekt Tic-Tac-Toe final move determination

This example project was created as a semestral project for subject SSP

## Authors

- Daniel Dobeš DOB0136 @DobesDaniel
- Daniel Honus HON0102 @honus

## Used for

Program is used to determine winning move of player on turn in game Tic-Tac-Toe.

## How to use

Program is used on data in a file containing the board,
where the actual state of the game Tic-Tac-Toe is stored.
Board contains data about each position on board by
storing one of three characters

1. '.' for free spaces
1. 'o' for moves made by player 1
1. 'x' for moves made by player 2

File is passed to program as an argument by file path.
Program is called by the following command:
> python tic_tac_toe.py "file path"

## Requirements

- Python
- Data stored in file (path is passed as an argument)

## Unit test reports

Read the [report](https://dobesdaniel.gitlab.io/ssp-projekt/report.html) here

## Documentation

Read the [documentation here](https://dobesdaniel.gitlab.io/ssp-projekt/)

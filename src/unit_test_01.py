import unittest

import tic_tac_toe

class TestUser(unittest.TestCase):

    def test_run(self):
        self.assertTrue(tic_tac_toe.main("data/playground.txt") == (3, 0))
        
    def test_corrent_load(self):
        data = [['.', 'x', 'o', '.', 'o', 'o', '.'], 
        ['o', 'o', '.', '.', 'o', '.', 'x'], 
        ['.', 'x', '.', 'x', 'x', 'x', '.'], 
        ['.', '.', 'x', '.', 'o', 'x', '.'], 
        ['o', '.', 'o', '.', 'o', 'x', '.'], 
        ['o', '.', 'x', 'x', 'x', '.', 'x'], 
        ['o', 'x', 'o', 'o', '.', '.', 'x'], 
        ['o', 'o', '.', '.', 'o', '.', 'x'], 
        ['x', '.', '.', 'x', 'x', 'o', 'o']]
        self.assertTrue(tic_tac_toe.loadData("data/playground.txt") == data)
    
    def test_count(self):
        data = [['.', 'x', 'o', '.', 'o', 'o', '.'], 
        ['o', 'o', '.', '.', 'o', '.', 'x'], 
        ['.', 'x', '.', 'x', 'x', 'x', '.'], 
        ['.', '.', 'x', '.', 'o', 'x', '.'], 
        ['o', '.', 'o', '.', 'o', 'x', '.'], 
        ['o', '.', 'x', 'x', 'x', '.', 'x'], 
        ['o', 'x', 'o', 'o', '.', '.', 'x'], 
        ['o', 'o', '.', '.', 'o', '.', 'x'], 
        ['x', '.', '.', 'x', 'x', 'o', 'o']]
        
        result = tic_tac_toe.count(data[0])
        self.assertTrue(result == (41, 39))
        result = tic_tac_toe.count(data[5])
        self.assertFalse(result == (41, 39))
    
    def test_invalid(self):
        with self.assertRaises(Exception):
            self.assertTrue(tic_tac_toe.main("data/bad.txt") == (0, 0))
        

if __name__ == '__main__':
    unittest.main()


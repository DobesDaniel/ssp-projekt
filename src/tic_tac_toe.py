"""
---------------------------------------------------------------

		Program: tic_tac_toe.py
		Version: 1.1
		Date: 16. 11. 2019

		Task: piskvorky - urcovani vitezneho zahu hrace na rade

		Comment
			- DONE - nacte matici ze souboru a prevod do pole
			- DONE - pocet O a poet X -> kdo je na tahu?
			- DONE - definovani smeru (vektoru) hledani
			- DONE - funkce na zjistovani rady 
				(zda je dostatecny pocet znaku a je na dalsi znak misto)
			- DONE - kontrola znaku - zmena vektoru -> pouziti fce
			- DONE - vypis vysledku

-----------------------------------------------------------------
"""

#nacteni hraciho pole ze souboru
#"""
import sys
"""
file_playground = sys.argv[1]
"""
file_playground = "data/playground.txt"


#--------------------------------
#definovani promennych s pocetem zahranych tahu + funkce na pricitani
countO = 0
countX = 0

def count(temp):	#vypocet poctu symbolu
	global countO
	global countX
	
	for char in temp:
		if char == "o":
			countO += 1
		
		elif char == "x":
			countX += 1
		
		else:
			pass
	
	return (countO, countX)

#--------------------------------
#nacteni hraciho pole do pole
def loadData(source):
	file = open(source, "r")
	playground = []		#vytvoreni pole s matici

	for line in file:
		row = list(map(str, line.split()))	#rozdeli precteny radek podle mezer
		playground.append(row)	#prida radek do 2D pole
		count(row)
	return playground
  
  
file = open(file_playground, "r")
playground = []		#vytvoreni pole s matici

for line in file:
	row = list(map(str, line.split()))	#rozdeli precteny radek podle mezer
	playground.append(row)	#prida radek do 2D pole
	count(row)

#print(playground[1][2])	#vypise znak na danych souradnicich
#print(playground)			#kontrolni vypis

#--------------------------------
#vypsani poctu zahranych tahu a vypsani toho, kdo je na tahu
#print("countO ", countO)
#print("countX ", countX)

if countO == countX:
	#print("O")
	char = "o"
else:
	#print("X")
	char = "x"

#--------------------------------
#nutne promenne pro funkci charcontrol
#vektory
right = [0,1]
rightdown = [1,1]
down = [1,0]
leftdown = [1, -1]

winlen = 5	#vyherni delka rady

rows, cols = len(playground), len(playground[0])
#print("rows, cols", rows, cols)

#--------------------------------
#funkce - rada hledanych znaku

def charcontrol(i,j):
	global playground
	global char
	global vektori, vektorj	#tohle by bylo asi vhodnejsi predavat s funkci
	global rows, cols
	global winlen
	idx = 0			#urcuje pozici znaku v rade
	
	while idx < winlen:		#chybi rade pouze 1 symbol?
		if ((i+(idx*vektori) < rows) and (j+(idx*vektorj) < cols)) and ((i+(idx*vektori) > -1) and (j+(idx*vektorj) > -1)) :	#podminka pri dosazeni okraje
			
			if playground[i+(idx*vektori)][j+(idx*vektorj)] == char:
				idx+=1
			
			else:
				break
		else:
			break
	
	if idx == winlen-1: 	#je mozne zahrat vitezny tah v dane rade
		
		if (i+(idx*vektori) < rows) and (j+(idx*vektorj) < cols) and (j+(idx*vektorj) > -1):	#podminky pri dosazeni hranic

			if playground[i+(idx*vektori)][j+(idx*vektorj)] == ".":	#kontrola, zda je na dane pozici volno
				return i+(idx*vektori), j+(idx*vektorj)		#vysledny tah se nachazi na konci rady
			
			elif (i-vektori > -1) and (playground[i-vektori][j-vektorj] == "."):
				return i-vektori, j-vektorj
		
		elif (i-vektori > -1) and (j-vektorj > -1) and (j-vektorj < cols):						#podminky pri dosazeni hranic

			if playground[i-vektori][j-vektorj] == ".":		#kontrola, zda je na dane pozici volno
				return i-vektori, j-vektorj					#vysledny tah se nachazi na zacatku rady

			elif (i+(idx*vektori) < rows) and (playground[i-vektori][j-vektorj] == "."):
				return i+(idx*vektori), j+(idx*vektorj)		

			
	return -1, -1		#neni mozne zahrat vitezny tah	
			

def main(data):		
	global playground
	global char
	global vektori, vektorj	#tohle by bylo asi vhodnejsi predavat s funkci
	global rows, cols
	global winlen
 
	loadData(data)
	#--------------------------------
	#algoritmus pro vyhledavani 4 znaku za sebou

	i = 0	#radek

	control = True

	while (i < rows) and control:	#vyhodnejsi je projet hraci pole jednou a menit vektory - pri velkych h polich. 4x rychlejsi
		j = 0	#sloupec

		while (j < cols) and control:
			if playground[i][j] == char:
				#print(playground[i][j],i,j)
				
				vektori, vektorj = right[0], right[1]	#myslim ze by to slo vyresit elegantneji, ale je to celkem rychle
				Ri, Rj = charcontrol(i,j)
				if Ri!=-1 and Rj!=-1:
					control = False
					break
							
				vektori, vektorj = rightdown[0], rightdown[1]	#nastaveni vektoru
				Ri, Rj = charcontrol(i,j)	#funkce vypis souradnice vyherniho tahu nebo -1 -1 pokud neexsituje
				if Ri!=-1 and Rj!=-1:		#pokud najde vysledny tah, ukonci vyhledavani
					control = False
					break
					
				vektori, vektorj = down[0], down[1]
				Ri, Rj = charcontrol(i,j)
				if Ri!=-1 and Rj!=-1:
					control = False
					break
				
				vektori, vektorj = leftdown[0], leftdown[1]
				Ri, Rj = charcontrol(i,j)
				if Ri!=-1 and Rj!=-1:
					control = False
					break
				
				
			j += 1	#inkrementace sledovaneho sloupce
		
		i += 1	#inkrementace sledovaneho radku

	return(Ri, Rj)		#vypis vysledku
	#--------------------------------
	#print("press enter to end")
	#input()

if __name__ == '__main__':
    loadData("../data/playground.txt")
    print(main("../data/playground2.txt"))
